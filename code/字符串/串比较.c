#include "dsstring.h" //请不要删除，否则检查不通过
#include <stdio.h>
#include <stdlib.h>

int str_compare(const char* ptr1, const char* ptr2)
{
    int dist1, dist2;
    int i = 0;
    while (ptr1[i] && ptr2[i]) {
        if (ptr1[i] == ptr2[i]) {
            i++;
            continue;
        } else {
            if (ptr1[i] <= 'Z') {
                dist1 = ptr1[i] - 'A';
            }
            if (ptr1[i] >= 'a') {
                dist1 = ptr1[i] - 'a';
            }
            if (ptr2[i] <= 'Z') {
                dist2 = ptr2[i] - 'A';
            }
            if (ptr2[i] >= 'a') {
                dist2 = ptr2[i] - 'a';
            }
            if (dist1 == dist2) {
                i++;
                continue;
            } else {
                if (ptr1[i] < ptr2[i]) {
                    return -1;
                } else {
                    return 1;
                }
            }
        }
    }
    if (ptr1[i] == 0 && ptr2[i] != 0) {
        return -1;
    } else if (ptr1[i] != 0 && ptr2[i] == 0) {
        return 1;
    } else {
        return 0;
    }
}
