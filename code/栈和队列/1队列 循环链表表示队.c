#include "list.h" // 请不要删除，否则检查不通过
#include <stdio.h>
#include <stdlib.h>

bool init_queue(LinkQueue* LQ)
{
    *LQ = (LinkQueue)malloc(sizeof(LinkQueueNode));
    if (*LQ != NULL) {
        (*LQ)->next = *LQ;
        return true;
    }
    return false;
}

bool enter_queue(LinkQueue* LQ, ElemType x)
{
    LinkQueue NewNode, temp;
    temp = (*LQ)->next;
    NewNode = (LinkQueue)malloc(sizeof(LinkQueueNode));
    if (NewNode != NULL) {
        NewNode->data = x;
        NewNode->next = temp;
        (*LQ)->next = NewNode;
        *LQ = NewNode;
        return true;
    }
    return false;
}

bool leave_queue(LinkQueue* LQ, ElemType* x)
{
    LinkQueue temp, temp2;
    if ((*LQ)->next != NULL && (*LQ)->next != *LQ) {
        temp = (*LQ)->next->next;
        *x = temp->data;
        (*LQ)->next->next = temp->next;
        temp2 = temp->next;
        free(temp);
        if (temp2->next == temp2)
            *LQ = temp2;
        return true;
    }
    return false;
}
