#include "list.h" // 请不要删除，否则检查不通过
#include <stdio.h>
#include <stdlib.h>

int compute_reverse_polish_notation(char *str)
{
  int i = 0;
  Stack Numbers;
  init_stack(&Numbers);
  ElemType number_to_push, num1, num2;
  while (str[i] != '\0')
  {
    if (str[i] != ' ')
    {
      if (str[i] >= '0' && str[i] <= '9')
      {
        number_to_push = 0;
        while (str[i] != ' ' && str[i])
        {
          number_to_push = number_to_push * 10 + (str[i] - '0');
          i++;
        }
        push(&Numbers, number_to_push);
      }
      else
      {
        pop(&Numbers, &num2);
        pop(&Numbers, &num1);
        switch (str[i])
        {
        case '+':
        {
          num1 += num2;
          break;
        }
        case '-':
        {
          num1 -= num2;
          break;
        }
        case '*':
        {
          num1 *= num2;
          break;
        }
        case '/':
        {
          num1 /= num2;
          break;
        }
        case '%':
        {
          num1 %= num2;
          break;
        }
        }
        push(&Numbers, num1);
      }
    }
    else
    {
      ;
    }
    i++;
  }
  pop(&Numbers, &num1);
  return num1;
}