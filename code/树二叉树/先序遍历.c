/*
 * 完成人　：曾文轩(2019091606011)
 * 完成时间：2020-05-06, Wed, 13:49:06
 * 最高分数：100
 */


#include "bitree.h" //请不要删除，否则检查不通过
#include <stdio.h>
#include <stdlib.h>

void pre_order(BiTree root)
{
    Stack S;
    init_stack(&S);
    if (root == NULL)
        return;
    push(&S, root);
    while (!is_empty(&S)) {
        BiTree p;
        pop(&S, &p);
        visit_node(p);
        if (p->right != NULL)
            push(&S, p->right);
        if (p->left != NULL)
            push(&S, p->left);
    }
}