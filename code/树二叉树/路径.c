/*
 * 完成人　：孙轶扬(2019091605025)
 * 完成时间：2020-05-06, Wed, 17:36:32
 * 最高分数：100
 */


#include "bitree.h" //请不要删除，否则检查不通过
#include <stdio.h>
#include <stdlib.h>

bool path(BiTNode* root, BiTNode* node, Stack* s)
{
    if (root == NULL || node == NULL)
        return false;
    BiTNode* p = root;
    BiTNode* pre = NULL;
    while (p != NULL || !is_empty(s)) {
        while (p != NULL) {
            push(s, p);
            if (p == node) {
                return true;
            }
            p = p->left;
        }
        if (!is_empty(s)) {
            top(s, &p);
            while (p->right == NULL || pre != NULL && p->right == pre) {
                pop(s, &pre);
                top(s, &p);
            }
            p = p->right;
        }
    }
    return false;
}