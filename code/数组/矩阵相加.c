/*
 * 完成人　：余子潇(2019091607019)
 * 完成时间：2020-04-14, Tue, 22:28:07
 * 最高分数：100
 */


#include "tsmatrix.h"
#include <stdio.h>
#include <stdlib.h>

bool add_matrix(const TSMatrix* pM, const TSMatrix* pN, TSMatrix* pQ)//这种写法不好看，但是能简单明了的解决问题，以后有机会可以写新算法
{
    int i = 0, j = 0, k = 0, temp;
    //用i、j、k来分别定位pM、pN、pQ
    if (pM->m != pN->m || pM->n != pN->n) //两矩阵大小不对，直接退出
        return false;

    pQ->m = pM->m; //下面是再给和矩阵初始化
    pQ->n = pM->n;
    pQ->len = 0;

    while (i < pM->len && j < pN->len && k < MAXSIZE) {
        if (pM->data[i].i < pN->data[j].i) { //M矩阵中元素的行号比较小
            pQ->data[k].i = pM->data[i].i;
            pQ->data[k].j = pM->data[i].j;
            pQ->data[k].e = pM->data[i].e;
            pQ->len++;
            k++;
            i++;
        } else if (pM->data[i].i == pN->data[j].i) { //M矩阵中元素的行号等于N中的行号
            if (pM->data[i].j == pN->data[j].j) { //两元素列号相等
                temp = pM->data[i].e + pN->data[j].e;
                if (temp == 0) { //如果值相抵消，就没了，继续往下找
                    i++;
                    j++;
                } else { //否则就合二为一
                    pQ->data[k].i = pM->data[i].i;
                    pQ->data[k].j = pM->data[i].j;
                    pQ->data[k].e = temp;
                    pQ->len++;
                    k++;
                    i++;
                    j++;
                }
            } else if (pM->data[i].j > pN->data[j].j) { //N矩阵中元素列号较小
                pQ->data[k].i = pN->data[j].i;
                pQ->data[k].j = pN->data[j].j;
                pQ->data[k].e = pN->data[j].e;
                pQ->len++;
                k++;
                j++;
            } else { //M矩阵中元素列号较小
                pQ->data[k].i = pM->data[i].i;
                pQ->data[k].j = pM->data[i].j;
                pQ->data[k].e = pM->data[i].e;
                pQ->len++;
                k++;
                i++;
            }
        } else { //N矩阵中元素的行号比较小
            pQ->data[k].i = pN->data[j].i;
            pQ->data[k].j = pN->data[j].j;
            pQ->data[k].e = pN->data[j].e;
            pQ->len++;
            k++;
            j++;
        }
    }

    if (i < pM->len) {
        while (i < pM->len && k < MAXSIZE) {
            pQ->data[k].i = pM->data[i].i;
            pQ->data[k].j = pM->data[i].j;
            pQ->data[k].e = pM->data[i].e;
            pQ->len++;
            k++;
            i++;
        }

    } else if (j < pN->len) {
        while (j < pN->len && k < MAXSIZE) {
            pQ->data[k].i = pN->data[j].i;
            pQ->data[k].j = pN->data[j].j;
            pQ->data[k].e = pN->data[j].e;
            pQ->len++;
            k++;
            j++;
        }
    }
    return true;
}
