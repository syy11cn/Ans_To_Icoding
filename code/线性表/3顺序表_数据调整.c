#include "list.h" // 请不要删除，否则检查不通过
#include <stdio.h>
#include <stdlib.h>

void odd_even(SeqList* L)
{
    SeqList T;
    int start = 0, end = L->last;
    for (int i = 0; i <= L->last; i++) {
        if (L->elem[i] % 2) {
            T.elem[start] = L->elem[i];
            start++;
        } else {
            T.elem[end] = L->elem[i];
            end--;
        }
    }
    T.last = L->last;
    *L = T;
}