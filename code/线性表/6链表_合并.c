#include "list.h" // 请不要删除，否则检查不通过
#include <stdio.h>
#include <stdlib.h>

void lnk_merge(LinkList A, LinkList B, LinkList C)
{
    Node *a, *b, *c;
    a = A->next;
    b = B->next;
    c = C;
    while (a != NULL || b != NULL) {
        if (a == NULL) {
            c->next = b;
            break;
        } else {
            c->next = a;
            a = a->next;
            c = c->next;
        }
        if (b == NULL) {
            c->next = a;
            break;
        } else {
            c->next = b;
            b = b->next;
            c = c->next;
        }
    }
}