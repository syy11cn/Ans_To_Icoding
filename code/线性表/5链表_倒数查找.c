#include "list.h" // 请不要删除，否则检查不通过
#include <stdio.h>
#include <stdlib.h>

int lnk_search(LinkList L, int k, ElemType* p_ele)
{
    LinkList p = L->next;
    ElemType a[100] = { 0 };
    int i = -1;
    while (p != NULL) {
        i++;
        a[i] = p->data;
        p = p->next;
    }
    if ((i - (k - 1)) >= 0) {
        *p_ele = a[i - (k - 1)];
        return 1;
    } else {
        return 0;
    }
}