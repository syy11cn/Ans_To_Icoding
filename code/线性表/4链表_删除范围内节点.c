#include "list.h" // 请不要删除，否则检查不通过
#include <stdio.h>
#include <stdlib.h>

void lnk_del_x2y(LinkList L, ElemType mink, ElemType maxk)
{
    LinkList p = L->next;
    LinkList prev = L;
    LinkList node;
    while (p != NULL) {
        if (p->data > mink && p->data < maxk) {
            prev->next = p->next;
            node = p;
            free(node);
            p = p->next;
        } else {
            prev = p;
            p = p->next;
        }
    }
}