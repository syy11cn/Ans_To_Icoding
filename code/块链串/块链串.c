/*
 * 完成人　：余子潇(2019091607019)
 * 完成时间：2020-04-15, Wed, 22:32:06
 * 最高分数：100
 */


#include "dsstring.h" // 请不要删除，否则检查不通过
#include <stdio.h>
#include <stdlib.h>

bool blstr_substr(BLString src, int pos, int len, BLString* sub)
{
    int start = pos / 4;
    int num = pos % 4;
    int i = 0;

    Block* p = src.head;
    Block* temp = (Block*)malloc(sizeof(Block));
    if (temp == NULL)
        return false;
    temp->next = NULL; //这个贼重要，如果没有这句的话，测试代码不知道什么时候结束，就会乱飘

    if (len <= 0 || pos > src.len - 1)
        return false;
    if (pos + len > src.len) {
        sub->len = len = src.len - pos;
    } else {
        sub->len = len;
    } //这一段代码是解决sub->len的取值问题，如果要的长度比原串剩余部分还长的话，就得砍掉多的尾巴

    sub->head = temp;
    for (; start > 0; start--) {
        p = p->next;
    } //找到起点

    while (len > 0) { //len为0的时候就是完成任务的时候
        if (i == 4) { //i用来控制子串内部容量，确定什么时候该加一个新节点
            Block* new = (Block*)malloc(sizeof(Block));
            if (new == NULL)
                return false;
            new->next = NULL;
            temp->next = new;
            temp = temp->next;
            i = 0;
        }

        if (num == 4) { //num用来控制原串跳到下一个节点
            p = p->next;
            num = 0;
        } else if (num < 4) {
            temp->ch[i] = p->ch[num];
            i++;
            num++;
            len--;
        }
    }
    if (i < 4) { //完成任务后如果最后一个节点没满，就填满它
        for (; i != 4; i++)
            temp->ch[i] = '#';
    }
    sub->tail = temp;
    return true;
}
